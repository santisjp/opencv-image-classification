#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <vector>
#include <string>

#include "histogram.h"
#include "LBP.h"
#include "OLBP.h"

#include <iostream>
#include <fstream>


void display_image() {
	
	cv::Mat img_input = cv::imread("image.orig/0.jpg", CV_LOAD_IMAGE_COLOR);
	cv::namedWindow("Input", cv::WINDOW_NORMAL);
	cv::imshow("Input", img_input);
	
	cv::waitKey();

}

void write_file(std::ofstream &FILE, std::vector<float> vector_val, std::string class_) {


	for (auto& value : vector_val) {
		FILE << value << ",";
	}


	FILE << class_ << std::endl;


}

void write_header(std::ofstream &FILE, std::vector<std::string> classes) {

	FILE << "@RELATION Classe_de_Imagens\n" << std::endl;
	
	int attribute_num;

	for (attribute_num = 0; attribute_num < 256; attribute_num++){
		FILE << "@ATTRIBUTE ";
		FILE << std::to_string(attribute_num);
		FILE << " NUMERIC" << std::endl;
	}

	FILE << "@ATTRIBUTE class {";


	for (int i = 0; i < classes.size(); i++) {

		if (i < classes.size() - 1) {
			FILE << classes[i] << ", ";
		}
		else {
			FILE << classes[i];
		}
	}

	FILE << "}\n" << std::endl;

	FILE << "@DATA" << std::endl;

}

std::vector<int> make_lbp(std::string image_file){
	
	cv::Mat img_output;
	cv::Mat img_input = cv::imread(image_file, CV_LOAD_IMAGE_COLOR);
	//cv::namedWindow("Input", cv::WINDOW_NORMAL);
	//cv::imshow("Input", img_input);

	cv::cvtColor(img_input, img_input, CV_BGR2GRAY);
	cv::GaussianBlur(img_input, img_input, cv::Size(3, 3), 5, 3, cv::BORDER_CONSTANT);

	lbplibrary::LBP *lbp;
	lbp = new lbplibrary::OLBP;	// 0-255
	
	lbp->run(img_input, img_output);
	
	double min, max; 
	cv::minMaxLoc(img_output, &min, &max); 
	//std::cout << "min: " << min << ", max: " << max;
	cv::normalize(img_output, img_output, 0, 255, cv::NORM_MINMAX, CV_8UC1);

	cv::Mat lbp_hist, lbp1_hist;
	int histSize[] = { 256 };
	float s_ranges[] = { 0, 256 };
	const float* ranges[] = { s_ranges };
	// Use the o-th and 1-st channels
	int channels[] = { 0 };
	
	
	std::vector<int> img_vector;

	if (img_output.isContinuous()) {
		img_vector.assign(img_output.datastart, img_output.dataend);
	}
	else {
		for (int i = 0; i < img_output.rows; ++i) {
			img_vector.insert(img_vector.end(), img_output.ptr<int>(i), img_output.ptr<int>(i) + img_output.cols);
		}
	}

	//cv::imshow("Output", img_output);
	//cv::waitKey();
	delete lbp;

	return img_vector;
}

std::vector<float> initiate_vector(std::vector<int> img_vector) {

	//std::cout << "Image vector: " << std::endl;
	int num_pos = 0;

	std::vector <float> rodo(256);

	int num_pixels = 0;

	for (auto& pos : img_vector) {
		rodo[pos]++;
		num_pixels++;
	}

	/*
	std::cout << ">>Vetor: " << std::endl;

	for (auto& valor : rodo) {

		std::cout << valor << std::endl;
	}

	std::cout << ">>rodo.size = " << rodo.size() << std::endl;

	*/

	for (int i = 0; i < rodo.size(); i++) {

		rodo[i] = rodo[i] / num_pixels * 100;
		//std::cout << "[" << i << "] = " << rodo[i] << std::endl;

	}

	return rodo;
}

int main(int argc, char **argv) {
	
	int MAX_IMAGES = 1000;

	std::ofstream FILE;
	FILE.open("image_class.arff");

	std::vector<std::string> classes{ "Indio", "Praia", "Lugares", "Busao", "Dinos", "Elefante", "Flor", "Cavalo", "Montanha", "Comida" };
	
	write_header(FILE, classes);

	std::vector<float> image_vector;
	std::string path("image.orig/");
	std::string extention(".jpg");
	std::string image_file;
	int image_num = 0;
	int class_num = 0;

	image_file.reserve(path.size() + extention.size() + 4);

	std::cout << "Calculating for " << MAX_IMAGES << " image(s)..." << std::endl;

	for (image_num = 0; image_num < MAX_IMAGES; image_num++) {

		image_file.append(path);
		image_file.append(std::to_string(image_num));
		image_file.append(extention);

		//std::cout << "Calculando " << image_file << " | Classe = " << classes[class_num] << std::endl;

		image_vector = initiate_vector(make_lbp(image_file));
		write_file(FILE, image_vector, classes[class_num]);

		image_file.clear();

		if (((image_num + 1) % 100) == 0) {
			class_num++;
		}

	}

	std::cout << "Done." << std::endl;

	FILE.close();
	std::system("pause");
	return 0;
}
